#include <stdio.h>
#include <string.h>
#include <stdlib.h>

typedef struct Node {
    int data;
    struct Node* next;
} Node;

Node* push(Node* list, int data) {
    Node* current = list;
    while (current->next != NULL) {
        current = current->next;
    }

    current->next = malloc(sizeof(Node));
    current->next->data = data;
    current->next->next = NULL;
    return list;
}

Node* range(int from, int to) {
     Node* list = NULL;
     Node* current = list;
     int i = from;
     while (i != to) {
         list =  push(list, i);      
         i++;
     }
     return list; 
}
void print_list(Node* list) {
   Node* current = list;

    while (current != NULL) {
        printf("%d\n", current->data);
        current = current->next;
    }
}



Node* reverseList(Node* list) {

    Node* current = list;
    Node* prev = NULL;

    while (current != NULL) {

        Node* next = current->next;
        current->next = prev;

        prev = current;
        current = next;
    }
    // prev contains the address of the first Node of the reversed list
    return prev;
}
int main () {
   return 0;
}

